<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title>Deployable probabistic programming</title>

    <link rel="stylesheet" href="css/reveal.css">
    <link rel="stylesheet" href="css/theme/white.css">

    <!-- Theme used for syntax highlighting of code -->
    <link rel="stylesheet" href="lib/css/arduino-light.css">

    <!-- Printing and PDF exports -->
    <script>
      var link = document.createElement( 'link' );
      link.rel = 'stylesheet';
      link.type = 'text/css';
      link.href = window.location.search.match( /print-pdf/gi ) ? 'css/print/pdf.css' : 'css/print/paper.css';
      document.getElementsByTagName( 'head' )[0].appendChild( link );
    </script>
    <style type="text/css">
        .reveal .slides section .static { visibility: hidden; height: 0; width: 0} /* replace with animated for PDF export */

        .reveal .slides section .highlight-current-bg-cyan,
        .reveal .slides section .highlight-current-bg-magenta,
        .reveal .slides section .fragment.highlight-current-bg-yellow,
        .reveal .slides section .highlight-bg-cyan,
        .reveal .slides section .highlight-bg-magenta,
        .reveal .slides section .fragment.highlight-bg-yellow {
           opacity: 1;
           visibility: inherit; }

        .reveal .slides section .highlight-bg-cyan.visible {
           background-color: cyan;
        }
        .reveal .slides section .highlight-bg-magenta.visible {
           background-color: magenta;
        }
        .reveal .slides section .fragment.highlight-bg-yellow.visible {
           background-color: yellow;
        }
        .reveal .slides section .highlight-current-bg-cyan.current-fragment {
           background-color: cyan;
        }
        .reveal .slides section .highlight-current-bg-magenta.current-fragment {
           background-color: magenta;
        }
        .reveal .slides section .fragment.highlight-current-bg-yellow.current-fragment {
           background-color: yellow;
        }
    </style>
  </head>
  <body>
    <div class="reveal">
      <!--
        <img src="infergopher.png" style="width: 1.5em; float:
        left; padding: 12pt">
      -->
      <div class="slides">
        <section>
          <h1>Deployable probabilistic programming</h1>
          <p><emph>David Tolpin, <a href="http://www.bgu.ac.il/">BGU</a> &amp; <a href="http://publus.com/">PUB+</a></emph></p>
          <p><a href="http://offtopia.net/infergo-talk/">http://offtopia.net/infergo-talk/</a></p>
          <p style="margin-top: 2em">press <tt style="border: thin solid black; padding: 0pt 3pt">Space</tt> to move through slides</p>
        </section>
        <section>
            <section>
                <h2>Probabilistic Programming</h2>
                Programs as statistical models:
                <pre><code class="javascript" data-trim data-noescape>
1 var breastCancer = flip(0.01)
2 var benignCyst = flip(0.2)
3 var positiveMammogram = (breastCancer && flip(0.8))
4                       || (benignCyst && flip(0.5))
5 condition(positiveMammogram)
6 return breastCancer
                </pre></code>
                <i>In the example:</i> breast cancer probability given
                positive mammogram.
            </section>
            <section>
                <h2>Probabilistic Programs</h2>
                <ul>
                    <li>Compute <i>posterior distributions.</i></li>
                    <li>May contain conditions, loops,
                        recursions.</li>
                    <li>Are (mostly) deterministic!</li>
                    <li>Must be run `backwards'.</li>
                    <li>(Approximate) inference algorithms are
                        required.</li>
                </ul>
            </section>
        </section>
        <section>
          <section>
            <h2>Probabilistic programming languages</h2>

            <ul>
              <li>45 PPLs listed on
                <a
                  href="http://en.m.wikipedia.org/wiki/Probabilistic_programming_language">Wikipedia</a>.</li>
              <li>18 PPLs participated in developer meetup at <a
                  href="http://probprog.cc/">PROBPROG
                  2018</a>.</li>
              <li>8 of the latter are not on Wikipedia (yet).</li>
            </ul>
          </section>

          <section>
            <h2>Probabilistic programming</h2>
            <p>Two polar views:</p>
            <ol>
              <li>Framework for statistical modelling</li>
              <li><i>Inferentiable</i> programming &#x2014; programs with inferrable parameters</li>
            </ol>
          </section>
        </section>
        <section>
          <section>
            <h2>Challenges</h2>
            <ul>
              <li>Simulation vs. inference</li>
              <li>Data</li>
              <li>Deployment</li>
            </ul>
          </section>
          <section>
            <h3>Simulation vs. inference</h3>
            <table width="110%">
              <tr>
                <td width="50%">
            <pre><code class="stan" data-trim data-noescape>
data {
    int<lower=0> N;
    vector[N] y;
    vector[N] x;
}

parameters {
    real alpha; real beta;
    real<lower=0> sigma;
}</code></pre>
                </td><td width="50%">
            <pre><code class="stan" data-trim data-noescape>model {
    alpha ~ normal(0,10);    
    beta ~ normal(0,10);
    sigma ~ cauchy(0,5);

    for (n in 1:N)
        y[n] ~ normal(
          <mark>alpha + beta * x[n]</mark>,
          sigma);
}</code></pre>
                </td></tr>
            </table>
          </section>
          <section>
            <h3>Data structures</h3>
            <table width="100%">
              <tr>
                <th width="30%">I mean:</th>
                <td width="70%">
            <pre><code class="python" data-trim style="font-size:70%" data-noescape>
def update_beliefs(beliefs, i, j, bandwidth):
    <span class="fragment highlight-current-bg-cyan" data-fragment-index="1">beliefs[i][j] += 1</span>
    evidence = sum(beliefs[i])
    if evidence > bandwidth:
        <span class="fragment highlight-current-bg-cyan" data-fragment-index="2">for j in range(len(beliefs[i])):
            beliefs[i][j] *= bandwidth / evidence</span>
      </code></pre>
      </td>
              </tr>
              <tr>
                <th>I write:</th>
                <td>
      <pre><code class="python" data-trim style="font-size:70%" data-noescape>
def update_beliefs(beliefs, i, j, bandwidth):
    <span class="fragment highlight-current-bg-yellow" data-fragment-index="1">update = torch.zeros(beliefs.shape)
    update[i, j] = 1
    beliefs = beliefs + update</span>
    evidence = beliefs[i, :].sum()
    if evidence > bandwidth:
        <span class="fragment highlight-current-bg-yellow" data-fragment-index="2">scale = torch.ones(beliefs.shape)
        scale[i, :] = bandwidth / evidence
        beliefs = beliefs * scale</span>
    return beliefs
            </code></pre>
            </td></tr>
            </table>
          </section>
          <section>
            <h3>Deployment</h3>
            <table>
              <tr>
                <th>PyStan (70Mb)</th>
                <td>
                  Stan, C++ compiler, Cython, <span style="color: #c66">OCaml?</span>
                </td>
              </tr>
              <tr>
                <th width="30%">
                  Pyro (&gt;600Mb)</th>
                <td>
                  <pre style="font-size: 50%">
Successfully installed contextlib2-0.5.5
decorator-4.3.0 graphviz-0.10.1 networkx-2.2
opt-einsum-2.3.2 pyro-ppl-0.3.0 six-1.12.0
torch-1.0.0 tqdm-4.29.1</pre>
                </td>
              </tr>
              <tr>
                <th>
                  Turing.jl (50Mb):
                </th>
                <td>
                  <pre style="font-size: 50%">
Installed NNlib ─────────── v0.4.3
Installed Colors ────────── v0.9.5
Installed Arpack ────────── v0.3.0
Installed BinaryProvider ── v0.5.3
Installed ForwardDiff ───── v0.10.1
Installed ColorTypes ────── v0.7.5
Installed ProgressMeter ─── v0.9.0
Installed ZipFile ───────── v0.8.0
Installed StaticArrays ──── v0.10.2
Installed Juno ──────────── v0.5.3
... (~40 packages)</pre>
              </td></tr>
            </table>
          </section>
        </section>
        <section>
          <h2>Guidelines</h2>
          <ul>
            <li>One language</li>
            <li>Common data structures</li>
            <li>Inference code re-used in simulation</li>
          </ul>
        </section>
        <section>
          <section>
            <h2>Probabilistic programming<br/>in Go</h2>
            <p style="text-align: left; margin-left: 2em">
            Go is
            </p>
            <ul>
              <li>small,</li>
              <li>expressive,</li>
              <li>efficient,</li>
              <li>popular for server-side programming.</li>
            </ul>
          </section>
          <section>
            <h3>'hello world': model</h3>
            <pre><code class="go" data-trim data-noescape>
 1  type Model struct {
 2      Data []float64
 3  }
 4  
 5  // x[0] is the mean, x[1] is the 
 6  // log stddev of the distribution
 7  func (m *Model) Observe(x []float64) float64 {
 8    // Our prior is a unit normal ...
 9    ll := Normal.Logps(0, 1, x...)
10    // ... but the posterior is based on data observations.
11    ll += Normal.Logps(x[0], math.Exp(x[1]), m.Data...)
12    return ll
13  }
            </code></pre>
          </section>
          <section>
            <h3>'hello world': data
            <pre><code class="go" data-trim data-noescape>
 1  m := &Model{[]float64{
 2    -0.854, 1.067, -1.220, 0.818, -0.749,
 3    0.805, 1.443, 1.069, 1.426, 0.308}}
            </code></pre>

          </section>
          <section>
            <h3>'hello world': optimization</h3>
            <pre><code class="go" data-trim data-noescape>
 1  x := []float64{0, 0}
 2    
 3  opt := &infer.Momentum{
 4    Rate:  0.01,
 5    Decay: 0.998,
 6  }
 7  for iter := 0; iter != 1000; iter++ {
 8    opt.Step(m, x)
 9  }
10  mean, logs = x[0], x[1]
            </code></pre>
          </section>
          <section>
            <h3>'hello world': posterior</h3>
            <pre><code class="go" data-trim data-noescape>
 1  x := []float64{0, 0}
 2    
 3  hmc := &infer.HMC{
 4    Eps: 0.1,
 5  }
 6  samples := make(chan []float64)
 7  hmc.Sample(m, x, samples)
 8  for i := 0; i != 1000; i++ {
 9    x = <-samples
10  }
11  hmc.Stop()
            </code></pre>
          </section>
          <section>
            <h3>'hello world': streaming</h3>
            <pre><code class="go" data-trim data-noescape>
 1  type Model struct {
 2    Data chan float64  // data is a channel
 3    N    int           // batch size
 4  }
 5  
 6  func (m *Model) Observe(x []float64) float64 {
 7    ll := Normal.Logps(0, 1, x...)
 8    // observe a batch of data from the channel
 9    for i := 0; i != m.N; i++ {
10      ll += Normal.Logp(x[0], math.Exp(x[1]), <- m.Data)
11    }
12    return ll
13  }
            </code></pre>
          </section>
          <section>
            <h3>Why Go?</h3>
            <ul>
              <li>Comes with parser and type checker.</li>
              <li>Compiles and runs fast.</li>
              <li>Allows efficient parallel execution, via
                <i>goroutines</i>.</li>
            </ul>
          </section>
        </section>
        <section>
          <section>
            <h2>Infergo</h2>
            <ul>
              <li>Models are written in Go.</li>
              <li>Relies on automatic differentiation for inference.</li>
              <li>Works anywhere where Go does.</li>
              <li>No external dependencies.</li>
              <li>Licensed under the MIT license.</li>
            </ul>
          </section>
          <section>
            <h3>Model</h3>
            Model interface:
            <pre><code class="go" data-trim data-noescape>
1 type Model interface {
2   Observe(parameters []float64) float64
3 }
            </code></pre>
            <div class="fragment">
            A model (exponential distribution):
            <pre><code class="go" data-trim data-noescape>
1 type Expon float64 
2 
3 func (m Expon) Observe(x []float64) float64 {
4   return -x*m
5 }
            </code></pre>
            </div>
          </section>
          <section>
            <h3>Distributions</h3>
            A distribution is a model:
            <pre><code class="go" data-trim data-noescape>
 1  var Normal normal
 2
 3  // Observe implements the Model interface.
 4  func (dist normal) Observe(x []float64) float64 {
 5    mu, sigma, y := x[0], x[1], x[2:]
 6    return dist.Logps(mu, sigma, y...)
 7  }
            </code></pre>
          </section>
          <section>
            <h3>Distributions (cont.)</h3>
            <pre><code class="go" data-trim data-noescape>
 8  // Logp computes the log pdf of a single observation.
 9  func (_ normal) Logp(mu, sigma float64, y float64)
10      float64 {
11    ...
12  }
13
14  // Logps computes the log pdf of a vector of observations.
15  func (_ normal) Logps(mu, sigma float64, y ...float64)
16      float64 {
17    ...
18  }
        </code></pre>
      </section>
      <section>
          <h3>Automatic differentiation</h3>
          <ul>
              <li>Numeric differentiation: $\frac {df} {dx} \approx \frac {f (x + \Delta x) - f(x)} {\Delta x}$</li>
              <li style="margin: 1em 0">Symbolic differentiation: $\frac {duv} {dx} = u\frac{dv} {dx} + v \frac{du} {dx}$</li>
              <li class="fragment"><b>Algorithmic differentiation</b>
                  <ul>
                      <li>Operator overloading.</li>
                      <li><b>Source code transformation.</b></li>
                  </ul>
              </li>
          </ul>
      </section>

      <section>
        <h3>Differentiation in Infergo</h3>
        <ul>
    <li>Model methods returning <code>float64</code>
      or nothing are differentiated.</li>
          <li>Within the methods, the following is differentiated:
            <ul>
              <li>assignments to <code>float64</code>;</li>
              <li>returns of <code>float64</code>;</li>
              <li>standalone calls to differentiated model
        methods.</li>
    </ul>
    </li>
        </ul>
      </section>
      <section>
        <h3>Differentiation in Infergo</h3>
        Reverse-mode via source code transformation:
        <pre><code class="go" data-trim data-noescape style="font-size: 80%">
1  func (m *Model) Observe(x []float64) float64 {
2    var ll float64
3    ad.Assignment(&ll, ad.Call(func(_ []float64) {
4      Normal.Logps(0, 0, x...)
5    }, 2, ad.Value(0), ad.Value(1)))
6    ad.Assignment(&ll, 
7      ad.Arithmetic(ad.OpAdd, &ll,
8        ad.Call(func(_ []float64) {
9          Normal.Logps(0, 0, m.Data...)
10        }, 2, &x[0], ad.Elemental(math.Exp, &x[1]))))
11    return ad.Return(&ll)
12  }
        </code></pre>
      </section>
          <section>
              <h3>Differentiation in Infergo</h3>
              <h4>Tape</h4>
              <pre><code class="go" data-trim>
 1  type adTape struct {
 2      records    []record    // recorded instructions
 3      places     []*float64  // variable places
 4      values     []float64   // stored values
 5      elementals []elemental // gradients of elementals
 6      cstack     []counters  // counter stack (see below)
 7  }
              </code></pre>

          </section>

          <section>
              <h3>Differentiation in Infergo</h3>
              <h4>Tape</h4>
              <pre><code class="go" data-trim style="font-size: 80%; line-height: 1.1">
 1  func Arithmetic(op int, px ...*float64) *float64 {
 2      tape := tapes.get()
 3      // Register
 4      p := Value(0)
 5      r := record{
 6          typ: typArithmetic,
 7          op:  op,
 8          p:   len(tape.places),
 9      }
10      tape.places = append(tape.places, p)
11      tape.places = append(tape.places, px...)
12      tape.records = append(tape.records, r)
13      // Run
14      switch op {
15      case OpNeg:
16          *p = -*px[0]
17          //  ...
18      }
19      return p
20  }
          </code></pre>
      </section>
      <section>
        <h2>Inference</h2>
        <ul>
          <li>Optimization via gradient ascent (Momentum,
            Adam), works with streamed and stochastic
            data.</li>
          <li>Full posterior inference via HMC variants.
            Samples are produced concurrently and passed
            through a channel.</li>
          <li class="fragment">Third-party inference algorithms.</li>
        </ul>
      </section>
      <section>
        <h3>Third-party inference</h3>
        <ul>
          <li>Straightforward because Infergo uses built-in
            Go <code>float64</code>.</li>
          <li>Gonum (<a href="http://gonum.org/">http://gonum.org/</a>) library for
            numerical computation:
            <pre><code class="go" data-trim data-noescape>
func FuncGrad(m Model) (
Func func(x []float64) float64,
Grad func(grad []float64, x []float64))
            </code></pre>
          </li>
        </ul>
      </section>
    </section>
    <section>
      <section>
        <h2>Examples</h2>
        <ul>
          <li>Simple models are still simple.</li>
          <li>Complex models are less complex.</li>
        </ul>
      </section>
      <section>
        <h3>8 schools</h3>
        <table width="110%">
          <tr><th width="45%">Stan</th><th width="55%">Infergo</th></tr>
          <tr style="font-size: 70%">
            <td>
            <pre><code class="stan" data-trim data-noescape>
data {
int&lt;lower=0&gt; J;
vector[J] y;
vector&lt;lower=0&gt;[J] sigma;
}

parameters {
real mu;
real&lt;lower=0&gt; tau;
vector[J] eta;
}

transformed parameters {
vector[J] theta;
theta = mu + tau * eta;
}

model {
eta ~ normal(0, 1);
y ~ normal(theta, sigma);
}

        </code></pre>
            </td><td>
        <pre><code class="go" data-trim data-noescape>
type Model struct {
J          int
Y          []float64
Sigma      []float64
}

func (m *Model) Observe(x []float64) float64 {
mu := x[0]
tau := math.Exp(x[1])
eta := x[2:]

ll := Normal.Logp(0, 1, eta)
for i, y := range m.Y {
theta := mu + tau*eta[i]
ll += Normal.Logp(theta, m.Sigma[i], y)
}
return ll
}
        </code></pre>
            </td></tr></table>
      </section>
      <section>
        <h3>Linear regression</h3>
        <table width="110%">
          <tr style="font-size: 65%"><td width="50%">
        <pre><code class="go" data-trim data-noescape>
type Model struct {
Data  [][]float64
}

func (m *Model) Observe(x []float64)
float64 {
ll := 0.

alpha, beta := x[0], x[1]
sigma := math.Exp(x[2])

for i := range m.Data {
ll += Normal.Logp(
  <span class="fragment highlight-current-bg-yellow" data-fragment-index="1">m.Simulate</span>(m.Data[i][0], alpha, beta),
  sigma, m.Data[i][1])
}
...
        </code></pre>
            </td><td width="50">
        <pre><code class="go" data-trim data-noescape>
...
return ll
}

// Simulate predicts y for x based on
// inferred parameters.
func (m *Model) <span class="fragment highlight-current-bg-yellow" data-fragment-index="1">Simulate</span>(x, alpha, beta float64)
float64 {
y := alpha +beta*x
return y
}
        </code></pre>
            </td></tr></table>
      </section>
      <section>
        <h3>Latent Dirichlet allocation</h3>
        <table width="100%">
          <tr><th width="50%">Infergo (0.5s|8.9s)</th><th width="50%">Stan (54s|3.7s)</th></tr>
          <tr style="font-size: 45%">
            <td>
            <pre><code class="go" data-trim data-noescape>
type LDAModel struct {
K     int       // num topics
V     int       // num words
M     int       // num docs
N     int       // total word instances
Word  []int     // word n
Doc   []int     // doc for word n
Alpha []float64 // topic prior
Beta  []float64 // word prior
}

func (m *LDAModel) Observe(x []float64) float64 {
ll := Normal.Logps(0, 1, x...)
theta := make([][]float64, m.M)
D.Simplices(&x, m.K, theta)
phi := make([][]float64, m.K)
D.Simplices(&x, m.V, phi)

// priors
ll += Dirichlet{m.K}.Logps(m.Alpha, theta...)
ll += Dirichlet{m.V}.Logps(m.Beta, phi...)

gamma := make([]float64, m.K)
for in := 0; in != m.N; in++ {
for ik := 0; ik != m.K; ik++ {
  gamma[ik] = math.Log(theta[m.Doc[in]-1][ik]) +
    math.Log(phi[ik][m.Word[in]-1])
}
ll += D.LogSumExp(gamma)
}
return ll
}

        </code></pre>
            </td><td>
        <pre><code class="stan" data-trim data-noescape>
data {
int<lower=2> K;               // num topics
int<lower=2> V;               // num words
int<lower=1> M;               // num docs
int<lower=1> N;           // total word instances
int<lower=1,upper=V> w[N];    // word n
int<lower=1,upper=M> doc[N];  // doc for word n
vector<lower=0>[K] alpha;     // topic prior
vector<lower=0>[V] beta;      // word prior
}


parameters {
simplex[K] theta[M]; // topic dist for doc m
simplex[V] phi[K];   // word dist for topic k
}


model {
for (m in 1:M)  
theta[m] ~ dirichlet(alpha);  // prior
for (k in 1:K)  
phi[k] ~ dirichlet(beta);     // prior

for (n in 1:N) {
real gamma[K];
for (k in 1:K) 
  gamma[k] <- log(theta[doc[n],k]) 
    + log(phi[k,w[n]]);
increment_log_prob(log_sum_exp(gamma));
}
}
        </code></pre>
            </td></tr></table>
      </section>
    </section>
    <section>
      <h2>Acknowledgements</h2>
      <ul>
        <li><a href="https://www.cs.ubc.ca/~fwood/">Frank Wood</a> introduced me to
          probabilistic programming.</li>
        <li><a href="http://www.ccs.neu.edu/home/jwvdm/">Jan-Willem van de Meent</a>
discussed with me motives, ideas, and implementation
choices behind Infergo.</li>
        <li><a href="http://pubplus.com/">PUB+</a> supported me in
          development of Infergo.</li>
      </ul>
    </section>
    <section>
      <h2>Thank you!</h2>
      <img src="infergopher.png" alt="Gehenna Gopher"
        style="height:10em; border:none"/>
    </section>
  </div>
</div>

<script src="lib/js/head.min.js"></script>
<script src="js/reveal.js"></script>

<script>
  // More info about config & dependencies:
  // - https://github.com/hakimel/reveal.js#configuration
  // - https://github.com/hakimel/reveal.js#dependencies
  Reveal.initialize({
      transition: 'fade',
      math: {
          mathjax: 'MathJax/MathJax.js',
          config: 'TeX-AMS_HTML-full'
      },
      dependencies: [
          { src: 'plugin/markdown/marked.js' },
          { src: 'plugin/markdown/markdown.js' },
          { src: 'plugin/notes/notes.js', async: true },
          { src: 'plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } },
          { src: 'plugin/math/math.js', async: true }
      ]
  });
</script>
</body>
</html>
